import java.awt.Color;

/**
 * A painter for the background of tiles.
 *
 * @author Daniel Tang
 * @since 21 May 2017
 */
@FunctionalInterface
public abstract interface SliderTheme {

	/**
	 * Themes the specified tile.
	 *
	 * @param posX The x-coordinate of the tile.
	 * @param posY The y-coordinate of the tile.
	 * @param tileX The x-coordinate of the solved tile.
	 * @param tileY The y-coordinate of the solved tile.
	 * @param themeMoves Whether the theme moves with the tile.
	 * @param color1 The first {@link Color}.
	 * @param color2 The second {@link Color}.
	 * @return The {@link Color} that the tile hsould be painted.
	 */
	public abstract Color themeTile(
			final int posX, final int posY,
			final int tileX, final int tileY,
			final boolean themeMoves,
			final Color color1, final Color color2);
}
