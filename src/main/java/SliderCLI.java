import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.AppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.ui.CommandDrivenCLI;
import me.danielzgtg.compsci11_sem2_2017.common.ui.ConsoleUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.Launcher;
import me.danielzgtg.compsci11_sem2_2017.common.ui.PrintMessenger;
import me.danielzgtg.compsci11_sem2_2017.common.ui.Prompter;

/**
 * A command-line implementation of slider. Mainly for debugging.
 *
 * @author Daniel Tang
 * @since 14 May 2017
 */
public final class SliderCLI extends AppContainer {

	/**
	 * The underlying {@link CommandDrivenCLI} that this {@link SliderCLI} uses.
	 */
	private /*final*/ CommandDrivenCLI<SliderBackendMovable> backend;

	private static final Map<String, BiFunction<CommandDrivenCLI<SliderBackendMovable>,
			String[], SliderBackendMovable>> COMMANDS;

	/**
	 * The {@link Prompter} for obtaining user input.
	 */
	private final Prompter prompter;

	/**
	 * The {@link PrintMessenger} for showing the user some data.
	 */
	private final PrintMessenger messenger;

	static {
		final Map<String, BiFunction<CommandDrivenCLI<SliderBackendMovable>, String[], SliderBackendMovable>> commands
				= new HashMap<>();

		commands.put("show", (container, args) -> {
			final SliderBackendMovable state = container.getState();

			final List<List<String>> displayList = new LinkedList<>();

			final int width = state.getWidth();
			final int height = state.getHeight();
			{
				final List<String> headerDisplayList = new LinkedList<>();
				headerDisplayList.add("");
				for (int x = 0; x < width; x++) {
					headerDisplayList.add("x" + x);
				}
				displayList.add(headerDisplayList);
			}
			for (int y = 0; y < height; y++) {
				final List<String> rowDisplayList = new LinkedList<>();
				rowDisplayList.add("y" + y);

				for (int x = 0; x < width; x++) {
					rowDisplayList.add(String.valueOf(state.getTileAtPos(x, y)));
				}

				displayList.add(rowDisplayList);
			}

			container.getMessenger().println(ConsoleUtils.formatTable(displayList,
					'-', '|', '+', true));

			return state;
		});

		commands.put("move", (container, args) -> {
			final SliderBackendMovable state =		 container.getState();

			if (args.length != 3) {
				container.getMessenger().println("Usage: move x y");
				return state;
			}

			final int x, y;
			try {
				x = Integer.parseInt(args[1]);
				y = Integer.parseInt(args[2]);
			} catch (final NumberFormatException nfe) {
				container.getMessenger().println("Failed to parse coordinates!");
				return state;
			}

			container.getMessenger().println(state.pushTile(x, y) ? "Tile moved." : "Board Unchanged!");

			return state;
		});

		commands.put("gapadjacent", (container, args) -> {
			final SliderBackendMovable state = container.getState();

			if (args.length != 3) {
				container.getMessenger().println("Usage: gapadjacent x y");
				return state;
			}

			final int x, y;
			try {
				x = Integer.parseInt(args[1]);
				y = Integer.parseInt(args[2]);
			} catch (final NumberFormatException nfe) {
				container.getMessenger().println("Failed to parse coordinates!");
				return state;
			}

			container.getMessenger().println(state.isGapNextTo(x, y) ? "The gap is adjacent." : "The gap is not adjacent.");

			return state;
		});

		commands.put("restart", (container, args) -> {
			final SliderBackendMovable state = container.getState();

			if (args.length != 3) {
				container.getMessenger().println("Usage: restart xSize ySize");
				return state;
			}

			final int xSize, ySize;
			try {
				xSize = Integer.parseInt(args[1]);
				ySize = Integer.parseInt(args[2]);
			} catch (final NumberFormatException nfe) {
				container.getMessenger().println("Failed to parse coordinates!");
				return state;
			}

			container.getMessenger().println("Starting new game.");

			return new SliderBackendMovableSimple(xSize, ySize);
		});

		commands.put("scramble", (container, args) -> {
			final SliderBackendMovable state = container.getState();

			if (args.length != 2) {
				container.getMessenger().println("Usage: scramble times");
				return state;
			}

			final int times;
			try {
				times = Integer.parseInt(args[1]);
			} catch (final NumberFormatException nfe) {
				container.getMessenger().println("Failed to parse times!");
				return state;
			}

			final int[] validMoveBuffer = new int[8];

			BiConsumer<Integer, Integer> debugPrintCallback = null;
			if (times > 10000) {
				container.getMessenger().println("Too many moves, skipping debug prints.");
			} else {
				debugPrintCallback = (x, y) -> container.getMessenger().formatln("Simulated move at: (%d,%d)", x, y);
			}

			state.scramble(times, new Random(), debugPrintCallback);

			return state;
		});

		CommandDrivenCLI.addQuitFunctions(SliderConfig.SLIDER_CONSTANT_DATA, commands);

		COMMANDS = Collections.unmodifiableMap(commands);
	}

	public SliderCLI(final Prompter prompter, final PrintMessenger messenger) {
		final int[] validMoveBuffer = new int[8];
		this.backend = new CommandDrivenCLI<>(SliderConfig.SLIDER_CONSTANT_DATA,
				null, null,
				COMMANDS, new SliderBackendMovableSimple(4, 4), true,
				null, (container) -> {
			final SliderBackendMovable state = container.getState();

			container.getMessenger().println("Now Solved?: " + state.isSolved());
			container.getMessenger().println(String.format("Gap is at (%d,%d)", state.getGapX(), state.getGapY()));

			final int indices = state.copyGapAdjacentTiles(validMoveBuffer) << 1; // * 2 for x and y
			for (int i = 0; i < indices;) {
				container.getMessenger().formatln("Movable piece: (%d %d)",
						validMoveBuffer[i++], validMoveBuffer[i++]);
			}

			return state;
		}, null,
				null, null, null, null,
				prompter, messenger);

		this.prompter = prompter;
		this.messenger = messenger;
	}

	@Override
	protected void doLaunch() {
		// Run in different thread, because launching should not block the calling thread.
		new Thread(this::doRun).start();
	}

	private void doRun() {
		new Launcher(SliderConfig.SLIDER_CONSTANT_DATA, this.prompter, this.messenger)
				.safeLaunchConsoleWrappedMainLooping(
						(ignore) -> this.backend.run(), null
				);
		this.backend = null;
	}

	public static void main(String[] args) {
		final PrintMessenger messenger = new PrintMessenger(System.out);
		new SliderCLI(new Prompter(SliderConfig.SLIDER_CONSTANT_DATA, System.in, messenger), messenger).launch();
	}
}
