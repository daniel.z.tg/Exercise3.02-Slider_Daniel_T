import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import me.danielzgtg.compsci11_sem2_2017.common.platform.ResourceUtils;

/**
 * Holds Slider configs.
 *
 * @author Daniel Tang
 * @since 14 May 2017
 */
@SuppressWarnings("WeakerAccess")
public final class SliderConfig {

	/**
	 * Constants for slider.
	 */
	public static final Map<?, ?> SLIDER_CONSTANT_DATA;

	/**
	 * Config for slider.
	 */
	public static final Map<Object, Object> SLIDER_CONFIG = new HashMap<>();

	/**
	 * The prefix for constants that are copied to the config initially.
	 */
	private static final String DEFAULT_MIGRATE_PREFIX = "slider.defaultOption.";

	static {
		SLIDER_CONSTANT_DATA = loadConstantsMap();

		for (final Map.Entry entry : SLIDER_CONSTANT_DATA.entrySet()) {
			final Object key = entry.getKey();

			if (key instanceof String) {
				final String strKey = (String) key;

				if (strKey.startsWith(DEFAULT_MIGRATE_PREFIX)) {
					SLIDER_CONFIG.put(strKey.substring(DEFAULT_MIGRATE_PREFIX.length()), entry.getValue());
				}
			}
		}
	}

	/**
	 * Loads the constants {@link Map},
	 * based off {@link ResourceUtils#COMMON_CONSTANT_DATA}, and the local constants resource.
	 *
	 * @return The constants {@link Map}.
	 */
	@SuppressWarnings("unchecked")
	private static final Map<?, ?> loadConstantsMap() {
		final Map<?, ?> localMap =
				ResourceUtils.loadConstantsMap("slider.constants",
						ResourceUtils.getResourceLoaderForClass(SliderConfig.class));
		final Map<?, ?> result = new HashMap<>();

		result.putAll((Map) ResourceUtils.COMMON_CONSTANT_DATA);
		result.putAll((Map) localMap);

		return Collections.unmodifiableMap(result);
	}

	@Deprecated
	private SliderConfig() { throw new UnsupportedOperationException(); }
}
