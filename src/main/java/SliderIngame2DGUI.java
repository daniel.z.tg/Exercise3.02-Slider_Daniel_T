import java.util.Random;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

import me.danielzgtg.compsci11_sem2_2017.common.Coerce;
import me.danielzgtg.compsci11_sem2_2017.common.StringUtils;
import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.AppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.platform.ResourceUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.GraphicsUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.GridManager;
import me.danielzgtg.compsci11_sem2_2017.common.ui.SwingUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.Widgets;
import me.danielzgtg.compsci11_sem2_2017.common.ui.WindowKeyTracker;

/**
 * A simple Swing-based implementation of the slider game.
 *
 * @author Daniel Tang
 * @since 18 May 2017
 */
@SuppressWarnings({"ConstantConditions", "unchecked"})
public final class SliderIngame2DGUI extends AppContainer {

	private final JPanel contentPanel;
	private final JPanel growContentPanel;
	private final GridManager gridManager;

	private final JButton[] actionButtons;
	private final SliderBackendMovable backend;
	private final SliderTheme theme = ((SliderThemes) Coerce.asEnum(
			SliderThemes.class, SliderConfig.SLIDER_CONFIG.get("slider.theme"))).engine;
	private final BufferedImage[][] gridImages;

	private final Color color1 = new Color(Coerce.asInt(SliderConfig.SLIDER_CONFIG.get("slider.color1")));
	private final Color color2 = new Color(Coerce.asInt(SliderConfig.SLIDER_CONFIG.get("slider.color2")));

	/**
	 * Whether the theme moves.
	 */
	private final boolean themeMoves = Coerce.asBool(SliderConfig.SLIDER_CONFIG.get("slider.themeMoves"));

	/**
	 * Whether to invert the keyboard control axes.
	 */
	private final boolean invertKeyAxis = Coerce.asBool(SliderConfig.SLIDER_CONFIG.get("slider.invertAxis"));

	/**
	 * The user-facing index offset.
	 */
	private final int indexAdd = Coerce.asBool(SliderConfig.SLIDER_CONFIG.get("slider.zeroIndex")) ? 0 : 1;

	private final SliderDifficulty difficulty = (SliderDifficulty)
			Coerce.asEnum((Class) SliderDifficulty.class, SliderConfig.SLIDER_CONFIG.get("slider.difficulty"));
	private final String infoLayout = Coerce.asString(SliderConfig.SLIDER_CONSTANT_DATA.get(
			"slider." + difficulty.name().toLowerCase() + "InfoLayout"));
	private final JLabel difficultyInfo;
	private int movesLeft;
	private Random rng = new Random();

	private Runnable endGameFunc;

	/**
	 * The initial width of the app's {@link JFrame}.
	 */
	private final int initialWindowWidth;

	/**
	 * The initial height of the app's {@link JFrame}.
	 */
	private final int initialWindowHeight;

	public SliderIngame2DGUI() {
		final int width = Coerce.asInt(SliderConfig.SLIDER_CONFIG.get("slider.xsize"));
		final int height = Coerce.asInt(SliderConfig.SLIDER_CONFIG.get("slider.ysize"));

		final String themePath = Coerce.asString(SliderConfig.SLIDER_CONFIG.get("slider.themePath"));

		if (StringUtils.containsNonWhitespace(themePath)) {
			final BufferedImage image = ResourceUtils.loadImgSafe(themePath, ResourceUtils.EXTERNAL_RESOURCE_LOADER);

			this.gridImages = image == null ? null : GraphicsUtils.splitImage(image, width, height);
		} else {
			this.gridImages = null;
		}

		this.backend = new SliderBackendMovableSimple(width, height);

		while(this.backend.isSolved()) {
			this.backend.scramble();
		}

		this.contentPanel = new JPanel();
		SwingUtils.setFlowDirection(this.contentPanel, false);
		this.growContentPanel = SwingUtils.wrapInGrowingJPanel(this.contentPanel);

		this.actionButtons = new JButton[width * height];

		this.gridManager = new GridManager(width, height);
		this.contentPanel.add(this.gridManager.gridPanel);

		final Widgets widgets =
				new Widgets(SliderConfig.SLIDER_CONSTANT_DATA, ResourceUtils.COMMONLIB_RESOURCE_LOADER);

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				final int currentX = x;
				final int currentY = y;

				final JButton actionButton = widgets.createRegularButton();
				// Workaround: Button needs to be transparent except for selection indications
				SwingUtils.makeTransparent(actionButton);

				final JPanel cellPanel = this.gridManager.getCellPanel(currentX, currentY);
				cellPanel.add(actionButton);
				// Workaround: Set the color on the JPanel underneath instead
				this.actionButtons[(currentY * width) + currentX] = actionButton;

				// Hook event
				SwingUtils.hookButtonPress(actionButton, () -> {
					final int lastGapX = this.backend.getGapX();
					final int lastGapY = this.backend.getGapY();

					if (this.backend.pushTile(currentX, currentY)) {
						this.updatePosition(currentX, currentY);
						this.updatePosition(lastGapX, lastGapY);

						if (this.difficulty != SliderDifficulty.HARD) {
							onUpdate();
						}
					}

					if (this.difficulty == SliderDifficulty.HARD) {
						onUpdate();
					}
				});

				// Initial Update
				this.updatePosition(currentX, currentY);
			}
		}

		this.contentPanel.add(this.difficultyInfo = widgets.createRegularLabel());

		final int area = width * height;
		switch (this.difficulty) {
			case EASY:
				this.movesLeft = Integer.MAX_VALUE;
				this.difficultyInfo.setText(this.infoLayout);
				break;
			case NORMAL:
				this.movesLeft = area * area;
				this.difficultyInfo.setText(String.format(this.infoLayout, this.movesLeft));
				break;
			case HARD:
				this.movesLeft = area * area;
				this.difficultyInfo.setText(String.format(this.infoLayout, this.movesLeft));
				break;
		}

		// Load Dimensions
		this.initialWindowWidth = Math.max(SwingUtils.MAGIC_MINIMUM_GOOD_JFRAME_SIZE,
				Coerce.asInt(SliderConfig.SLIDER_CONSTANT_DATA.get("slider.2dgui.initialWindowWidth")));
		this.initialWindowHeight = Math.max(SwingUtils.MAGIC_MINIMUM_GOOD_JFRAME_SIZE,
				Coerce.asInt(SliderConfig.SLIDER_CONSTANT_DATA.get("slider.2dgui.initialWindowHeight")));
	}

	/**
	 * Debounce for {@link SliderIngame2DGUI#onUpdate()}.
	 */
	private boolean alive = true;

	private final synchronized void onUpdate() {
		if (!this.alive) return;

		if (this.backend.isSolved()) {
			if (this.endGameFunc != null) {
				this.alive = false;
				JOptionPane.showMessageDialog(this.contentPanel, "Congratulations!");
				this.endGameFunc.run();
			}
		} else if (this.movesLeft == 0) {
			if (this.endGameFunc != null) {
				this.alive = false;
				this.difficultyInfo.setText(String.format(this.infoLayout, 0));
				JOptionPane.showMessageDialog(this.contentPanel, "Out of Moves!");
				this.endGameFunc.run();
			}
		} else if (this.difficulty != SliderDifficulty.EASY) {
			this.difficultyInfo.setText(String.format(this.infoLayout, this.movesLeft));
			this.movesLeft--;

			if (this.difficulty == SliderDifficulty.HARD) {
				if (this.rng.nextBoolean()) {
					directionalPush(this.rng.nextBoolean(), this.rng.nextBoolean());
				}
			}
		}
	}

	private final void updatePosition(final int x, final int y) {
		final int value = this.backend.getTileAtPos(x, y);

		final JPanel cellPanel = this.gridManager.getCellPanel(x, y);
		final int width = this.backend.getWidth();
		final JButton cellButton = this.actionButtons[(y * width) + x];
		if (value == -2) {
			throw new AssertionError();
		} else if (value == -1) {
			if (this.gridImages != null) {
				cellButton.setIcon(null);
			}

			cellPanel.setBackground(Color.BLACK);
			cellButton.setText("");
			cellButton.setEnabled(false);
		} else {
			final int tileX = value % width;
			final int tileY = value / width;

			if (this.gridImages != null) {
				cellButton.setIcon(new ImageIcon(this.gridImages[tileX][tileY]));
				cellButton.setText("");
			} else {
				cellButton.setText(String.valueOf(this.backend.getTileAtPos(x, y) + this.indexAdd));
			}
			cellPanel.setBackground(theme.themeTile(x, y,
					tileX, tileY,
					this.themeMoves, this.color1, this.color2));

			cellButton.setEnabled(true);
		}
	}

	private final void directionalPush(final boolean horizontal, final boolean downRight) {
		final int lastGapX = this.backend.getGapX();
		final int lastGapY = this.backend.getGapY();

		if (this.backend.pushAdjacentTile(
				horizontal, this.invertKeyAxis ^ downRight)) {
			this.updatePosition(this.backend.getGapX(), this.backend.getGapY());
			this.updatePosition(lastGapX, lastGapY);

			if (this.difficulty != SliderDifficulty.HARD) {
				onUpdate();
			}
		}

		if (this.difficulty == SliderDifficulty.HARD) {
			onUpdate();
		}
	}

	@Override
	protected void doLaunch() {
		// Setup window
		final JFrame frame = new JFrame() {
			{
				this.setTitle(Coerce.asString(SliderConfig.SLIDER_CONSTANT_DATA.get(
						"me.danielzgtg.compsci11_sem2_2017.common.mainMenu.title")));
			}

			@SuppressWarnings("unused") // Need to meet project requirements
			final JButton[] jbuttons = new JButton[] {
					new JButton(), new JButton(), new JButton(), new JButton(),
					new JButton(), new JButton(), new JButton(), new JButton(),
					new JButton(), new JButton(), new JButton(), new JButton(),
					new JButton(), new JButton(), new JButton(), new JButton()
			};
		};

		// Allow going back to main menu
		this.endGameFunc = () -> {
			frame.dispose();
			SliderGUI.launchMainMenu();
		};

		SwingUtils.hookJFrameClosing(frame, () -> {
			// In hard mode, the game cannot be closed
			if (this.difficulty != SliderDifficulty.HARD) {
				this.endGameFunc.run();
			}

			return false;
		});

		// Make the game window more aggressive
		if (this.difficulty == SliderDifficulty.HARD) {
			frame.setUndecorated(true);
			frame.setAlwaysOnTop(true);
			frame.setType(JFrame.Type.UTILITY);
			frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		}

		// Add key hooks
		WindowKeyTracker.hookKeyCodeDown(frame, (key) -> {
			switch (key) {
				case KeyEvent.VK_UP:
				case KeyEvent.VK_W:
					this.directionalPush(false, false);
					break;
				case KeyEvent.VK_LEFT:
				case KeyEvent.VK_A:
					this.directionalPush(true, false);
					break;
				case KeyEvent.VK_DOWN:
				case KeyEvent.VK_S:
					this.directionalPush(false, true);
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_D:
					this.directionalPush(true, true);
					break;
			}
		});

		// Setup frame contents
		SwingUtils.setupJFrameContents(frame, new Component[] {
				this.growContentPanel
		}, (panel) -> new BoxLayout(panel, BoxLayout.Y_AXIS));

		// Launch
		SwingUtils.finishJFrameSetup(frame, this.initialWindowWidth, this.initialWindowHeight);
		SwingUtils.launchJFrame(frame);
	}
}
