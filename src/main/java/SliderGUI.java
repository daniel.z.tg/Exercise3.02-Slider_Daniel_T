import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BooleanSupplier;

import me.danielzgtg.compsci11_sem2_2017.common.Coerce;
import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.impl.MainMenuGraphicalAppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.impl.SettingMeta;
import me.danielzgtg.compsci11_sem2_2017.common.appcontainer.impl.SettingsGraphicalAppContainer;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.CommonLambdas;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;
import me.danielzgtg.compsci11_sem2_2017.common.platform.PlatformUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.SwingUtils;

/**
 * Main controls for GUI implementations of slider.
 *
 * @author Daniel Tang
 * @since 14 May 2017
 */
@SuppressWarnings("unchecked")
public final class SliderGUI {

	public static void main(final String[] ignore) {
		PlatformUtils.setHighPerformance(true);

		if (PlatformUtils.getHighPerformance()) {
			SwingUtils.setupSystemLookAndFeel();
		}

		launchConfig();
	}

	/**
	 * Options in main  menu.
	 */
	private static final List<Pair<String, Pair<String, BooleanSupplier>>> MAIN_MENU_CONTROLS;

	/**
	 * List of settings.
	 */
	private static final List<Pair<String, SettingMeta<?>>> SETTINGS_META;

	static {
		final List<Pair<String, Pair<String, BooleanSupplier>>> tmpMainMenuControls = new LinkedList<>();

		tmpMainMenuControls.add(new Pair<>("slider.playButton", new Pair<>(
				"me.danielzgtg.compsci11_sem2_2017.common.mainMenu.play", () -> {
			new SliderIngame2DGUI().launch();

			return true;
		})));

		tmpMainMenuControls.add(new Pair<>("slider.configButton", new Pair<>(
				"slider.settings", () -> {
			launchConfig();

			return true;
		})));

		MAIN_MENU_CONTROLS = Collections.unmodifiableList(tmpMainMenuControls);

		final List<Pair<String, SettingMeta<?>>> tmpSettingsMeta = new LinkedList<>();

		tmpSettingsMeta.add(new Pair<>("General", new SettingMeta<>(
				SliderDifficulty.class, "slider.difficulty", "Difficulty",
				(x) -> x instanceof SliderDifficulty, x -> Coerce.asString(SliderConfig.SLIDER_CONSTANT_DATA.get(
					"slider." + ((SliderDifficulty) x).name().toLowerCase() + "Desc"))
		, (x) -> { throw new AssertionError(); })));

		tmpSettingsMeta.add(new Pair<>("General", new SettingMeta<>(
				Boolean.class, "slider.invertAxis", "Invert Axis Keyboard",
				CommonLambdas.TRUE_PREDICATE,
				x -> ((Boolean) x) ? "Keyboard drags tiles" : "Keyboard moves gap",
				(x) -> { throw new AssertionError(); })));

		tmpSettingsMeta.add(new Pair<>("Board", new SettingMeta<>(
				Integer.class, "slider.xsize", "Width",
				(x) -> ((Number) x).intValue() > 2,
				CommonLambdas.NULL_FUNCTION,
				(x) -> "Invalid Size")));

		tmpSettingsMeta.add(new Pair<>("Board", new SettingMeta<>(
				Integer.class, "slider.ysize", "Height",
				(x) -> ((Number) x).intValue() > 2,
				CommonLambdas.NULL_FUNCTION,
				(x) -> "Invalid Size")));

		tmpSettingsMeta.add(new Pair<>("Theme", new SettingMeta<>(
				Boolean.class, "slider.themeMoves", "Theme Moves",
				CommonLambdas.TRUE_PREDICATE,
				CommonLambdas.NULL_FUNCTION,
				(x) -> { throw new AssertionError(); })));

		tmpSettingsMeta.add(new Pair<>("Theme", new SettingMeta<>(
				SliderThemes.class, "slider.theme", "Theme",
				CommonLambdas.TRUE_PREDICATE,
				CommonLambdas.NULL_FUNCTION,
				(x) -> { throw new AssertionError(); })));

		tmpSettingsMeta.add(new Pair<>("Theme", new SettingMeta<>(
				Boolean.class, "slider.zeroIndex", "Zero-index",
				CommonLambdas.TRUE_PREDICATE,
				CommonLambdas.NULL_FUNCTION,
				(x) -> { throw new AssertionError(); })));


		tmpSettingsMeta.add(new Pair<>("Theme", new SettingMeta<>(
				String.class, "slider.themePath", "Image Path",
				CommonLambdas.TRUE_PREDICATE,
				CommonLambdas.NULL_FUNCTION,
				(x) -> { throw new AssertionError(); })));

		tmpSettingsMeta.add(new Pair<>("Theme", new SettingMeta<>(
				Integer.class, "slider.color1", "Color 1 (0xRRGGBB)",
				CommonLambdas.TRUE_PREDICATE,
				CommonLambdas.NULL_FUNCTION,
				(x) -> { throw new AssertionError(); })));


		tmpSettingsMeta.add(new Pair<>("Theme", new SettingMeta<>(
				Integer.class, "slider.color2", "Color 2 (0xRRGGBB)",
				CommonLambdas.TRUE_PREDICATE,
				CommonLambdas.NULL_FUNCTION,
				(x) -> { throw new AssertionError(); })));

		SETTINGS_META = Collections.unmodifiableList(tmpSettingsMeta);
	}

	/**
	 * Shows the main menu.
	 */
	public static final void launchMainMenu() {
		new MainMenuGraphicalAppContainer(SliderConfig.SLIDER_CONSTANT_DATA, MAIN_MENU_CONTROLS).launch();
	}

	/**
	 * Shows the settings.
	 */
	public static final void launchConfig() {
		new SettingsGraphicalAppContainer(SliderConfig.SLIDER_CONSTANT_DATA, SliderConfig.SLIDER_CONFIG,
				null, SETTINGS_META, (x) -> {
			if (x != null) {
				SliderConfig.SLIDER_CONFIG.putAll(x);
				launchMainMenu();
			} else {
				launchConfig();
			}
		}).launch();
	}
}
