import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.ui.GraphicsUtils;

/**
 * The built-in themes for slider.
 *
 * @author Daniel Tang
 * @since 21 May 2017
 */
public enum SliderThemes {

	XSTRIPES1((px, py, tx, ty, m, c1, c2) -> ((m ? tx : px) & 1) == 1 ? c1 : c2),
	XSTRIPES2((px, py, tx, ty, m, c1, c2) -> ((m ? tx : px) & 1) == 0 ? c1 : c2),
	YSTRIPES1((px, py, tx, ty, m, c1, c2) -> ((m ? ty : py) & 1) == 1 ? c1 : c2),
	YSTRIPES2((px, py, tx, ty, m, c1, c2) -> ((m ? ty : py) & 1) == 0 ? c1 : c2),
	CHECKERBOARD((px, py, tx, ty, m, c1, c2) -> ((m ? (tx ^ ty) : (px ^ py)) & 1) == 0 ? c1 : c2),
	RANDOM((px, py, tx, ty, m, c1, c2) -> Math.random() >= 0.5D ? c1 : c2),
	DAZZLE((px, py, tx, ty, m, c1, c2) -> GraphicsUtils.randomColor());

	public final SliderTheme engine;

	private SliderThemes(final SliderTheme engine) {
		Validate.notNull(engine);

		this.engine = engine;
	}
}
