import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * A simple backend implementation of a playable slider game.
 *
 * @author Daniel Tang
 * @since 14 May 2017
 */
public final class SliderBackendMovableSimple implements SliderBackendMovable {

	/**
	 * The width of the board.
	 */
	public final int width;

	/**
	 * The height of the board.
	 */
	public final int height;

	/**
	 * The tile data.
	 *
	 * Contains 0 to width * height - 1, for tiles, and -1 for the gap.
	 */
	private final int[] data;

	/**
	 * The x-coordinate of the gap.
	 */
	private int gapX;

	/**
	 * The y-coordinate of the gap.
	 */
	private int gapY;

	/**
	 * Creates a new simple backend implementation of a playable slider game.
	 *
	 * @param width The width of the board.
	 * @param height The height of the board.
	 */
	public SliderBackendMovableSimple(final int width, final int height) {
		Validate.require(width > 0);
		Validate.require(height > 0);

		this.width = width;
		this.height = height;

		this.data = new int[width * height];

		int position = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				this.setTileAtPos(x, y, position++);
			}
		}

		this.setTileAtPos(this.gapX = width - 1, this.gapY = height - 1, -1);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getWidth() {
		return this.width;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getHeight() {
		return this.height;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getTileAtPos(final int x, final int y) {
		if (x < 0 || x >= this.width || y < 0 || y >= this.height) {
			return -2;
		}

		return this.data[(y * this.width) + x];
	}

	/**
	 * Actually sets the tile at the specified position to specific tile data,
	 * without error checking, used internally.
	 *
	 * @param x The x-coordinate of the tile.
	 * @param y The y-coordinate of the tile.
	 * @param newData The new tile data.
	 */
	private void setTileAtPos(final int x, final int y, final int newData) {
		this.data[(y * this.width) + x] = newData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getGapX() {
		return this.gapX;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getGapY() {
		return this.gapY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean pushTile(final int x, final int y) {
		if (x < 0 || x >= this.width || y < 0 || y >= this.height) {
			return false;
		}

		if (this.isGapNextTo(x, y)) {
			// Move this tile to its position, and move gap to this tile.
			this.setTileAtPos(this.gapX, this.gapY, this.getTileAtPos(x, y));
			this.setTileAtPos(this.gapX = x, this.gapY = y, -1);

			return true;
		} else {
			return false;
		}
	}
}
