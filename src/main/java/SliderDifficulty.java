/**
 * The difficulties of a slider game.
 *
 * @author Daniel Tang
 * @since 21 May 2017
 */
public enum SliderDifficulty {
	EASY,
	NORMAL,
	HARD;
}
