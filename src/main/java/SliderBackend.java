import me.danielzgtg.compsci11_sem2_2017.common.Validate;

/**
 * A backend for a slider game.
 *
 * @author Daniel Tang
 * @since 14 May 2017
 */
public abstract interface SliderBackend {

	/**
	 * Gets the width of the board.
	 *
	 * @return The width of the board.
	 */
	public abstract int getWidth();

	/**
	 * Gets the height of the board.
	 *
	 * @return The height of the board.
	 */
	public abstract int getHeight();

	/**
	 * Gets the tile at the specified position.
	 *
	 * @param x The x-coordinate of the tile.
	 * @param y The y-coordinate of the tile.
	 * @return The tile at the specified position.
	 */
	public abstract int getTileAtPos(final int x, final int y);

	/**
	 * Gets the x-coordinate of the gap.
	 *
	 * @return Gets the x-coordinate of the gap.
	 */
	public default int getGapX() {
		final int width = this.getWidth();
		final int height = this.getHeight();

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (this.getTileAtPos(x, y) == -1) {
					return x;
				}
			}
		}

		throw new IllegalStateException("Gap doesn't seem to exist!");
	}

	/**
	 * Gets the y-coordinate of the gap.
	 *
	 * @return The y-coordinate of the gap.
	 */
	public default int getGapY() {
		final int width = this.getWidth();
		final int height = this.getHeight();

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (this.getTileAtPos(x, y) == -1) {
					return y;
				}
			}
		}

		throw new IllegalStateException("Gap doesn't seem to exist!");
	}

	/**
	 * Checks whether the gap is next to a tile.
	 *
	 * @param x The x-coordinate of the tile.
	 * @param y The y-coordinate of the tile.
	 * @return {@code true} if the gap is next to the tile.
	 */
	public default boolean isGapNextTo(final int x, final int y) {
		final int gapDiffX = this.getGapX() - x;
		final int gapDiffY = this.getGapY() - y;

		return (gapDiffX * gapDiffX) + (gapDiffY * gapDiffY) == 1;
	}

	/**
	 * Determines whether the board is solved.
	 *
	 * @return {@code true} if the board is solved
	 */
	public default boolean isSolved() {
		final int width = this.getWidth();
		final int height = this.getHeight();

		int position = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (this.getTileAtPos(x, y) != position++ && (x != width - 1 || y != height - 1)) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Copies the positions of adjacent tiles into a buffer.
	 *
	 * @param storage The buffer.
	 * @return The number of adjacent tiles.
	 */
	public default int copyGapAdjacentTiles(final /*mutable*/ int[] storage) {
		Validate.notNull(storage);
		Validate.require(storage.length == 8);

		final int width = this.getWidth();
		final int height = this.getHeight();
		final int gapX = this.getGapX();
		final int gapY = this.getGapY();

		int index = 0;
		if (gapX > 0) {
			storage[index++] = gapX - 1;
			storage[index++] = gapY;
		}
		if (gapX < width - 1) {
			storage[index++] = gapX + 1;
			storage[index++] = gapY;
		}
		if (gapY > 0) {
			storage[index++] = gapX;
			storage[index++] = gapY - 1;
		}
		if (gapY < height - 1) {
			storage[index++] = gapX;
			storage[index++] = gapY + 1;
		}

		return index >>> 1; // "index / 2" == # of stored spaces
	}

}
