import java.util.Random;
import java.util.function.BiConsumer;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.CommonLambdas;

/**
 * A playable backend for a slider game.
 *
 * @author Daniel Tang
 * @since 14 May 2017
 */
@SuppressWarnings("WeakerAccess")
public abstract interface SliderBackendMovable extends SliderBackend {

	/**
	 * Pushes the tile at the specified position.
	 *
	 * @param x The x-coordinate of the tile.
	 * @param y The y-coordinate of the tile.
	 * @return {@code true} if the board has changed.
	 */
	public abstract boolean pushTile(final int x, final int y);

	/**
	 * Pushes a tile adjacent to the gap.
	 *
	 * @param horizontal {@code true} to push along the x-axis.
	 * @param downRight {@code true} to push towards the 4th cartesian quadrant.
	 * @return {@code true} if the board has changed.
	 */
	public default boolean pushAdjacentTile(final boolean horizontal, final boolean downRight) {
		// Push tile adjacent to the gap, on the opposite direction, in the direction specified
		final int axisDirection = downRight ? 1 : -1;

		if (horizontal) {
			return this.pushTile(this.getGapX() + axisDirection, this.getGapY());
		} else {
			return this.pushTile(this.getGapX(), this.getGapY() + axisDirection);
		}
	}

	/**
	 * Scrambles the board, using default settings.
	 */
	public default void scramble() {
		// By default, scramble (x+y)**2 times.
		final int area = this.getWidth() * this.getHeight();

		this.scramble(area * area);
	}

	/**
	 * Scrambles the board, a specified number of times.
	 *
	 * @param times The number of times to scramble.
	 */
	public default void scramble(final int times) {
		this.scramble(times, new Random());
	}

	/**
	 * Scrambles the board, a specified number of times, using a provided {@link Random}.
	 *
	 * @param times The number of times to scramble.
	 * @param rng The {@link Random} to use to scramble.
	 */
	public default void scramble(final int times, final Random rng) {
		this.scramble(times, rng, null);
	}

	/**
	 * Scrambles the board, a specified number of times, using a provided {@link Random},
	 * informing a callback every time a tile is moved.
	 *
	 * @param times The number of times to scramble.
	 * @param rng The {@link Random} to use to scramble.
	 * @param callback The callback for when a tile is moved, {@code null} to disable.
	 */
	public default void scramble(final int times, final Random rng, final BiConsumer<Integer, Integer> callback) {
		Validate.notNull(times);

		final int[] validMoveBuffer = new int[8];

		for (int i = 0; i < times; i++) {
			final int moves = this.copyGapAdjacentTiles(validMoveBuffer);
			final int selectedMove = rng.nextInt(moves);

			int index = selectedMove << 1; // * 2
			final int x = validMoveBuffer[index++];
			final int y = validMoveBuffer[index];

			if (this.pushTile(x, y)) {
				if (callback != null) {
					callback.accept(x, y);
				}
			} else {
				throw new RuntimeException("Couldn't simulate move???");
			}
		}
	}
}
